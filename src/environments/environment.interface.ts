export interface IEnvironment {
  googleMapsApiKey: string;
  issLocationApiUrl: string;
  pixabayApiKey: string;
  pixabayApiUrl: string;
  production: boolean;
}
