import {
  PixabayColorsEnum,
  PixabayLanguagesEnum,
  PixabayCategoriesEnum,
  PixabayOrientationsEnum,
  PixabayImageTypesEnum,
  PixabayOrdersEnum } from '../enums';

export interface IPixaboyPhotoParams {
  /**
   *  API key
   */
  key: string;
  /**
   * A URL encoded search term. If omitted, all images are returned. This value may not exceed 100 characters.
   * Example?: "yellow+flower"
   */
  q?: string;
  /**
   * Language code of the language to be searched in.
   * @default LanguagesEnum.EN
   */
  lang?: PixabayLanguagesEnum;
  /**
   * Retrieve individual images by ID.
   */
  id?: string;
  /**
   * Filter results by image type.
   * @default PixabayImageTypesEnum.ALL
   */
  image_type?: PixabayImageTypesEnum;
  /**
   * Whether an image is wider than it is tall, or taller than it is wide.
   * @default PixabayOrientationsEnum.ALL
   */
  orientation?: PixabayOrientationsEnum;
  /**
   * Filter results by category
   */
  category?: PixabayCategoriesEnum;
  /**
   * Minimum image width.
   */
  min_width?: string;
  /**
   * Minimum image height.
   */
  min_height?: string;
  /**
   * Filter images by color properties. A comma separated list of values may be used to select multiple properties.
   */
  colors?: PixabayColorsEnum;
  /**
   * Select images that have received an Editor's Choice award.
   * @default false
   */
  editors_choice?: boolean;
  /**
   * A flag indicating that only images suitable for all ages should be returned.
   * @default false
   */
  safesearch?: boolean;
  /**
   * How the results should be ordered.
   * @default PixabayOrdersEnum.POPULAR
   */
  order?: PixabayOrdersEnum;
  /**
   * Returned search results are paginated. Use this parameter to select the page number.
   * @default 1
   */
  page?: number;
  /**
   * Determine the number of results per page.
   * Accepted values?: 3 - 200
   * @default 20
   */
  per_page?: number;
  /**
   * JSONP callback function name
   */
  callback?: string;
  /**
   * Indent JSON output. This option should not be used in production.
   * @default false
   */
  pretty?: boolean;
}
