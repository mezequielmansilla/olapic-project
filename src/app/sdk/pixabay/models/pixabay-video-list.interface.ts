import { IPixabayVideoHit } from './pixabay-video-hit.interface';

export interface IPixabayVideoList {
  total: number;
  totalHits: number;
  hits: IPixabayVideoHit[];
}
