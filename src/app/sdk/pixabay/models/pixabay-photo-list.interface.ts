import { IPixabayPhotoHit } from './pizabay-photo-hit.interface';

export interface IPixabayPhotoList {
  total: number;
  totalHits: number;
  hits: IPixabayPhotoHit[];
}
