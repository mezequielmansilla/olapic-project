import { IPixabayVideo } from './pixabay-video.interface';

export interface IPixabayVideoHit {
  id: number;
  pageURL: string;
  type: string;
  tags: string;
  duration: number;
  picture_id: string;
  videos: {
    large: IPixabayVideo,
    medium: IPixabayVideo,
    small: IPixabayVideo,
    tiny: IPixabayVideo,
  };
  views: number;
  downloads: number;
  favorites: number;
  likes: number;
  comments: number;
  user_id: number;
  user: string;
  userImageURL: string;
}
