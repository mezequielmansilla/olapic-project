import { NgModule } from '@angular/core';

import { PixabayPhotoApiService } from './services/pixabay-photo-api.service';
import { PixabayVideoApiService } from './services/pixabay-video-api.service';

@NgModule({
  declarations: [],
  imports: [],
  providers: [PixabayPhotoApiService, PixabayVideoApiService]
})
export class SdkPhotosModule { }
