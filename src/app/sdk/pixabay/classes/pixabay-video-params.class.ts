import { PixabayVideoTypesEnum } from '../enums';

import { PixabayBaseParams } from './pixabay-base-params.class';

export class PixabayVideoParams extends PixabayBaseParams {
  /**
   * Filter results by video type.
   * @default PixabayVideoTypesEnum.ALL
   */
  video_type?: PixabayVideoTypesEnum;
}