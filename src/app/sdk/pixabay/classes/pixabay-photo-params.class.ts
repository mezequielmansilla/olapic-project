import {
  PixabayColorsEnum,
  PixabayLanguagesEnum,
  PixabayCategoriesEnum,
  PixabayOrientationsEnum,
  PixabayImageTypesEnum,
} from '../enums';

import { PixabayBaseParams } from './pixabay-base-params.class';

export class PixaboyPhotoParams extends PixabayBaseParams{
  /**
   * Filter results by image type.
   * @default PixabayImageTypesEnum.ALL
   */
  image_type?: PixabayImageTypesEnum;
  /**
   * Whether an image is wider than it is tall, or taller than it is wide.
   * @default PixabayOrientationsEnum.ALL
   */
  orientation?: PixabayOrientationsEnum;
  /**
   * Filter images by color properties. A comma separated list of values may be used to select multiple properties.
   */
  colors?: PixabayColorsEnum;
}
