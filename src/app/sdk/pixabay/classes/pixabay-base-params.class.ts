import { PixabayOrdersEnum, PixabayLanguagesEnum, PixabayCategoriesEnum } from '../enums';

export class PixabayBaseParams {
  /**
   *  API key
   */
  key: string;
  /**
   * A URL encoded search term. If omitted, all images/videos are returned. This value may not exceed 100 characters.
   * Example?: "yellow+flower"
   */
  q?: string;
  /**
   * Language code of the language to be searched in.
   * @default LanguagesEnum.EN
   */
  lang?: PixabayLanguagesEnum;
  /**
   * Retrieve individual images/videos by ID.
   */
  id?: string;
  /**
   * Filter results by category
   */
  category?: PixabayCategoriesEnum;
  /**
   * Minimum image width.
   */
  min_width?: string;
  /**
   * Minimum image height.
   */
  min_height?: string;
  /**
   * Select videos that have received an Editor's Choice award.
   * @default false
   */
  editors_choice?: boolean;
  /**
   * A flag indicating that only videos suitable for all ages should be returned.
   * @default false
   */
  safesearch?: boolean;
  /**
   * How the results should be ordered.
   * @default PixabayOrdersEnum.POPULAR
   */
  order?: PixabayOrdersEnum;
  /**
   * Returned search results are paginated. Use this parameter to select the page number.
   * @default 1
   */
  page?: number;
  /**
   * Determine the number of results per page.
   * Accepted values?: 3 - 200
   * @default 20
   */
  per_page?: number;
  /**
   * JSONP callback function name
   */
  callback?: string;
  /**
   * Indent JSON output. This option should not be used in production.
   * @default false
   */
  pretty?: boolean;

  toParams(): { [parma: string]: string } {
    const params = {};
    Object.keys(this).forEach(key => {
      params[key] = this[key].toString();
    });
    return params;
  }
}
