import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { IPixabayVideoList } from '../models/pixabay-video-list.interface';
import { PixabayVideoParams } from '../classes/pixabay-video-params.class';

@Injectable()
export class PixabayVideoApiService {
  private readonly baseUrl = `${environment.pixabayApiUrl}/videos/`;

  constructor(private http: HttpClient) { }

  getVideosByLocation(locationName: string): Observable<IPixabayVideoList> {
    const pixabayParams = new PixabayVideoParams();
    pixabayParams.key = environment.pixabayApiKey;
    pixabayParams.q = locationName;
    const url = `${this.baseUrl}`;
    return this.http.get<IPixabayVideoList>(url, { params: pixabayParams.toParams() });
  }
}
