import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

import { IPixabayPhotoList } from '../models';
import { PixaboyPhotoParams } from '../classes/pixabay-photo-params.class';

@Injectable()
export class PixabayPhotoApiService {
  private readonly baseUrl = `${environment.pixabayApiUrl}/`;

  constructor(private http: HttpClient) { }

  getPhotosByLocationName(locationName: string): Observable<IPixabayPhotoList> {
    const pixabayParams = new PixaboyPhotoParams();
    pixabayParams.key = environment.pixabayApiKey;
    pixabayParams.q = locationName;
    const url = `${this.baseUrl}`;
    return this.http.get<IPixabayPhotoList>(url, { params: pixabayParams.toParams() });
  }
}
