export enum PixabayOrientationsEnum {
  ALL = 'all',
  HORIZONTAL = 'horizontal',
  VERTICAL = 'vertical',
}
