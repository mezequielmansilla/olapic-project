export enum PixabayLanguagesEnum {
  CS = 'cs',
  DA = 'da',
  DE = 'de',
  EN = 'en',
  ES = 'es',
  FR = 'fr',
  ID = 'id',
  IT = 'it',
  HU = 'hu',
  NL = 'nl',
  NO = 'no',
  PL = 'pl',
  PT = 'pt',
  RO = 'ro',
  SK = 'sk',
  FI = 'fi',
  SV = 'sv',
  TR = 'tr',
  VI = 'vi',
  TH = 'th',
  BG = 'bg',
  RU = 'ru',
  EL = 'el',
  JA = 'ja',
  KO = 'ko',
  ZH = 'zh',
}
