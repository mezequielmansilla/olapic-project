export enum PixabayOrdersEnum {
  POPULAR = 'popular',
  LATEST = 'latest',
}
