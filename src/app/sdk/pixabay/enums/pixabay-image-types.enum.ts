export enum PixabayImageTypesEnum {
  ALL = 'all',
  PHOTO = 'photo',
  ILLUSTRATION = 'illustration',
  VECTOR = 'vector',
}
