export enum PixabayVideoTypesEnum {
  ALL = 'all',
  FILM = 'film',
  ANIMATION = 'animation',
}
