export interface ICurrentISSLocation {
  message: string;
  iss_position: {
    latitude: string;
    longitude: string;
  };
  timestamp: number;
}
