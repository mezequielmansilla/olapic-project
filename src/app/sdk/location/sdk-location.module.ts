import { NgModule } from '@angular/core';

import { IssLocationApiService } from './services/iss-location-api.service';

@NgModule({
  declarations: [],
  imports: [],
  providers: [IssLocationApiService]
})
export class SdkLocationModule {}
