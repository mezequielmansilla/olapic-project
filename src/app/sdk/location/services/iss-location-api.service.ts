import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICurrentISSLocation } from '../models';
import { environment } from 'src/environments/environment';

@Injectable()
export class IssLocationApiService {
  private readonly baseUrl = environment.issLocationApiUrl;

  constructor(private http: HttpClient) {}

  /**
   * Current ISS location over Earth (latitude/longitude)
   */
  getCurrentISSLocation(): Observable<ICurrentISSLocation> {
    const url = `${this.baseUrl}/iss-now.json`;
    return this.http.get<ICurrentISSLocation>(url);
  }

}
