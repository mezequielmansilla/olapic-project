import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RoutesEnum } from './shared/enums';

@Component({
  selector: 'op-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Olapic Project';
  email = 'mezequielmansilla@gmail.com';

  constructor(private router: Router) { }

  ngOnInit() {}

  goHome() {
    this.router.navigate([RoutesEnum.HOME]);
  }

}
