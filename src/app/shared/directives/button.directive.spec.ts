import { ElementRef, Renderer2, Component } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';

import { ButtonDirective } from './button.directive';

@Component({
  template: '<button opButton>Test button</button>'
})
class TestComponent {
  constructor() { }
}

describe('ButtonDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [TestComponent, ButtonDirective],
      providers: [
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  it('should add the class `op-button`', () => {
    const debugEl: HTMLElement = fixture.debugElement.nativeElement;
    const button: HTMLElement = debugEl.querySelector('button');

    fixture.detectChanges();

    expect(button.classList).toContain('op-button');
  });
});
