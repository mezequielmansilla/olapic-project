import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[opButton]',
})
export class ButtonDirective {

  constructor(el: ElementRef, renderer: Renderer2) {
    renderer.addClass(el.nativeElement, 'op-button');
  }

}
