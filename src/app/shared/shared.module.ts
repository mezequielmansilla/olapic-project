import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonDirective } from './directives/button.directive';
import { LoadingComponent } from './components/loading/loading.component';

const declarations = [
  ButtonDirective,
  LoadingComponent,
];

@NgModule({
  declarations: [...declarations],
  imports: [],
  exports: [CommonModule, ...declarations],
  providers: []
})
export class SharedModule {}
