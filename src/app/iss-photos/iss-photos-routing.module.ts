import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IssPhotosComponent } from './iss-photos.component';


const routes: Routes = [
  { path: '', component: IssPhotosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IssPhotosRoutingModule { }
