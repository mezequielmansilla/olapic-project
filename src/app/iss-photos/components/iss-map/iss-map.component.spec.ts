import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgmMap, AgmMarker, AgmInfoWindow } from '@agm/core';

import { MockComponent } from 'ng-mocks';
import { IssMapComponent } from './iss-map.component';
import { LoadingComponent } from 'src/app/shared/components/loading/loading.component';

describe('IssMapComponent', () => {
  let component: IssMapComponent;
  let fixture: ComponentFixture<IssMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MockComponent(AgmMap),
        MockComponent(AgmMarker),
        MockComponent(AgmInfoWindow),
        IssMapComponent,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
