import { Component, OnInit, Input } from '@angular/core';
import { LatLngLiteral } from '@agm/core';

@Component({
  selector: 'op-iss-map',
  templateUrl: './iss-map.component.html',
  styleUrls: ['./iss-map.component.scss']
})
export class IssMapComponent implements OnInit {
  @Input()
  issLatitude;
  @Input()
  issLongitude;
  zoom = 7;

  paths: Array<LatLngLiteral> = [
    { lat: 0,  lng: 10 },
    { lat: 0,  lng: 20 },
    { lat: 10, lng: 20 },
    { lat: 10, lng: 10 },
    { lat: 0,  lng: 10 }
  ];

  constructor() { }

  ngOnInit() {
  }

}
