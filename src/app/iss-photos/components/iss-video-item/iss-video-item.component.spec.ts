import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssVideoItemComponent } from './iss-video-item.component';

describe('IssVideoItemComponent', () => {
  let component: IssVideoItemComponent;
  let fixture: ComponentFixture<IssVideoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssVideoItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssVideoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
