import { Component, OnInit, Input } from '@angular/core';

import { IPixabayVideoHit } from 'src/app/sdk/pixabay';

@Component({
  selector: 'op-iss-video-item',
  templateUrl: './iss-video-item.component.html',
  styleUrls: ['./iss-video-item.component.scss']
})
export class IssVideoItemComponent implements OnInit {
  @Input()
  video: IPixabayVideoHit;

  constructor() { }

  ngOnInit() {
  }

}
