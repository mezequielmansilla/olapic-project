import { Component, OnInit, Input } from '@angular/core';

import { forkJoin } from 'rxjs';

import { PixabayPhotoApiService, IPixabayPhotoHit, IPixabayVideoHit, PixabayVideoApiService } from 'src/app/sdk/pixabay';

@Component({
  selector: 'op-iss-media-list',
  templateUrl: './iss-media-list.component.html',
  styleUrls: ['./iss-media-list.component.scss']
})
export class IssMediaListComponent implements OnInit {
  @Input()
  locationNames: string[] = [];
  @Input()
  totalImages = 10;
  @Input()
  totalVideos = 5;

  loading = false;
  photos: IPixabayPhotoHit[] = [];
  videos: IPixabayVideoHit[] = [];

  constructor(private pixabayPhotoApiService: PixabayPhotoApiService, private pixabayVideoApiService: PixabayVideoApiService) { }

  ngOnInit() {
    this.loading = true;
    const locations = this.locationNames.toString();
    forkJoin(this.pixabayPhotoApiService.getPhotosByLocationName(locations), this.pixabayVideoApiService.getVideosByLocation(locations))
      .subscribe(([photos, videos]) => {
        this.loading = false;
        this.photos = photos.hits ? photos.hits.splice(0, this.totalImages) : [];
        this.videos = videos.hits ? videos.hits.splice(0, this.totalVideos) : [];
      }, error => this.loading = false);
  }

}
