import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MockComponent } from 'ng-mocks';
import { of } from 'rxjs';

import { PixabayPhotoApiService, PixabayVideoApiService } from 'src/app/sdk/pixabay';

import { LoadingComponent } from 'src/app/shared/components/loading/loading.component';
import { IssMediaListComponent } from './iss-media-list.component';
import { IssPhotoItemComponent } from '../iss-photo-item/iss-photo-item.component';
import { IssVideoItemComponent } from '../iss-video-item/iss-video-item.component';

describe('IssMediaListComponent', () => {
  let component: IssMediaListComponent;
  let fixture: ComponentFixture<IssMediaListComponent>;
  let mockPixabayPhotoApiService;
  let mockPixabayVideoApiService;

  beforeEach(async(() => {
    mockPixabayPhotoApiService = jasmine.createSpyObj('PixabayPhotoApiService', ['getPhotosByLocationName']);
    mockPixabayVideoApiService = jasmine.createSpyObj('PixabayVideoApiService', ['getVideosByLocation']);
    TestBed.configureTestingModule({
      declarations: [
        MockComponent(IssPhotoItemComponent),
        MockComponent(IssVideoItemComponent),
        MockComponent(LoadingComponent),
        IssMediaListComponent
      ],
      providers: [
        {
          provide: PixabayPhotoApiService,
          useValue: mockPixabayPhotoApiService,
        },
        {
          provide: PixabayVideoApiService,
          useValue: mockPixabayVideoApiService,
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssMediaListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    mockPixabayPhotoApiService.getPhotosByLocationName.and.returnValue(of([]));
    mockPixabayVideoApiService.getVideosByLocation.and.returnValue(of([]));
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
