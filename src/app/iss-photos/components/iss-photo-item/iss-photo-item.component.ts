import { Component, OnInit, Input } from '@angular/core';

import { IPixabayPhotoHit } from 'src/app/sdk/pixabay';

@Component({
  selector: 'op-iss-photo-item',
  templateUrl: './iss-photo-item.component.html',
  styleUrls: ['./iss-photo-item.component.scss']
})
export class IssPhotoItemComponent implements OnInit {
  @Input()
  photo: IPixabayPhotoHit;

  constructor() { }

  ngOnInit() {
  }

}
