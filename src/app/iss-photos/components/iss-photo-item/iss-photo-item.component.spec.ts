import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssPhotoItemComponent } from './iss-photo-item.component';

describe('IssPhotoItemComponent', () => {
  let component: IssPhotoItemComponent;
  let fixture: ComponentFixture<IssPhotoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssPhotoItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssPhotoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
