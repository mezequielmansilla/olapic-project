import { Component, OnInit } from '@angular/core';

import { AgmGeocoder, GeocoderRequest, GeocoderResult } from '@agm/core';
import { Observable, of } from 'rxjs';
import { tap, switchMap, filter, catchError } from 'rxjs/operators';

import { IssLocationApiService, ICurrentISSLocation } from '../sdk/location';

import { AlertService } from '../core/services/alert.service';

@Component({
  selector: 'op-iss-photos',
  templateUrl: './iss-photos.component.html',
  styleUrls: ['./iss-photos.component.scss']
})
export class IssPhotosComponent implements OnInit {
  loading = false;
  issLatitude;
  issLongitude;
  addresses: string[];

  constructor(
    private alertService: AlertService,
    private agmGeocoder: AgmGeocoder,
    private issLocationApiService: IssLocationApiService,
  ) { }

  ngOnInit() {
    this.issLocationInit();
  }

  private saveIssLocation(issLocation: ICurrentISSLocation) {
    if (!issLocation) {
      return;
    }
    this.issLatitude = +issLocation.iss_position.latitude;
    this.issLongitude = +issLocation.iss_position.longitude;
  }

  private geocode(issLatitude: number, issLongitude: number): Observable<GeocoderResult[]> {
    const request: GeocoderRequest = {
      location: {
        lat: issLatitude,
        lng: issLongitude,
      },
    };
    return this.agmGeocoder.geocode(request).pipe(catchError(error => of([])));
  }

  private issLocationInit() {
    this.loading = true;
    this.issLocationApiService.getCurrentISSLocation()
      .pipe(
        filter(response => !!response && !!response.iss_position),
        tap((response) => this.saveIssLocation(response)),
        tap(() => this.loading = false),
        switchMap(() => this.geocode(this.issLatitude, this.issLongitude))
      )
      .subscribe((geocoderResult) => {
        console.log('Geocode result', geocoderResult);
        this.addresses = geocoderResult.map(result => result.formatted_address);
      }, (error) => {
        this.loading = false;
        this.alertService.error('Error loading application.');
      });
  }

}
