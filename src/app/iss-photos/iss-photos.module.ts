import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

import { IssPhotosRoutingModule } from './iss-photos-routing.module';
import { IssPhotosComponent } from './iss-photos.component';
import { SharedModule } from '../shared/shared.module';

import { IssMapComponent } from './components/iss-map/iss-map.component';
import { IssPhotoItemComponent } from './components/iss-photo-item/iss-photo-item.component';
import { IssVideoItemComponent } from './components/iss-video-item/iss-video-item.component';
import { IssMediaListComponent } from './components/iss-media-list/iss-media-list.component';

@NgModule({
  declarations: [IssPhotosComponent, IssMapComponent, IssPhotoItemComponent, IssVideoItemComponent, IssMediaListComponent],
  imports: [
    SharedModule,
    AgmCoreModule,
    IssPhotosRoutingModule
  ]
})
export class IssPhotosModule { }
