import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgmGeocoder } from '@agm/core';
import { MockComponent } from 'ng-mocks';
import { of } from 'rxjs';

import { IssLocationApiService, ICurrentISSLocation } from '../sdk/location';

import { IssPhotosComponent } from './iss-photos.component';
import { LoadingComponent } from '../shared/components/loading/loading.component';
import { IssMapComponent } from './components/iss-map/iss-map.component';
import { IssMediaListComponent } from './components/iss-media-list/iss-media-list.component';
import { AlertService } from '../core/services/alert.service';

describe('IssPhotosComponent', () => {
  let component: IssPhotosComponent;
  let fixture: ComponentFixture<IssPhotosComponent>;
  let mockAlertService;
  let mockAgmGeocoder;
  let mockIssLocationApiService;

  beforeEach(async(() => {
    mockAlertService = jasmine.createSpyObj('AlertaService', ['error']);
    mockAgmGeocoder = jasmine.createSpyObj('AgmGeocoder', ['geocode']);
    mockIssLocationApiService = jasmine.createSpyObj(['getCurrentISSLocation']);

    TestBed.configureTestingModule({
      declarations: [
        MockComponent(LoadingComponent),
        MockComponent(IssMapComponent),
        MockComponent(IssMediaListComponent),
        IssPhotosComponent
      ],
      providers: [
        { provide: AlertService, useValue: mockAlertService },
        { provide: AgmGeocoder, useValue: mockAgmGeocoder },
        { provide: IssLocationApiService, useValue: mockIssLocationApiService }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssPhotosComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    mockIssLocationApiService.getCurrentISSLocation.and.returnValue(of(null));
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should load map component', () => {
    const response: ICurrentISSLocation = {
      iss_position: { latitude: '10', longitude: '11' },
      message: 'OK',
      timestamp: 100,
    };
    mockIssLocationApiService.getCurrentISSLocation.and.returnValue(of(response));

    fixture.detectChanges();

    const debugEl: HTMLElement = fixture.debugElement.nativeElement;
    const mapComponent: HTMLElement = debugEl.querySelector('op-iss-map');
    expect(mapComponent).toBeTruthy();
  });

  it('should load media list component', () => {
    const currentIssLocationresponse: ICurrentISSLocation = {
      iss_position: { latitude: '10', longitude: '11' },
      message: 'OK',
      timestamp: 100,
    };
    const geocodeResponse = [{
      formatted_address: 'test address'
    }];
    mockIssLocationApiService.getCurrentISSLocation.and.returnValue(of(currentIssLocationresponse));
    mockAgmGeocoder.geocode.and.returnValue(of(geocodeResponse));

    fixture.detectChanges();

    const debugEl: HTMLElement = fixture.debugElement.nativeElement;
    const mapComponent: HTMLElement = debugEl.querySelector('op-iss-media-list');
    expect(mapComponent).toBeTruthy();
    expect(component.addresses).toEqual(['test address']);
  });
});
