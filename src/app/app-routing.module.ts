import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoutesEnum } from './shared/enums';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: RoutesEnum.HOME },
  { path: RoutesEnum.HOME, loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  { path: RoutesEnum.ISS_PHOTOS, loadChildren: () => import('./iss-photos/iss-photos.module').then(m => m.IssPhotosModule) },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
