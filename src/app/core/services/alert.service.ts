import { Injectable } from '@angular/core';

@Injectable()
export class AlertService {

  constructor() { }

  success(message: string) {
    alert(`SUCCESS: ${message}`);
  }

  error(message: string) {
    alert(`ERROR: ${message}`);
  }

  info(message: string) {
    alert(`INFO: ${message}`);
  }
}
