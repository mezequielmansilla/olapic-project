import { NgModule } from '@angular/core';

import { SdkPhotosModule } from '../sdk/pixabay';
import { SdkLocationModule } from '../sdk/location';

import { AlertService } from './services/alert.service';

@NgModule({
  declarations: [],
  exports: [
    SdkLocationModule,
    SdkPhotosModule,
  ],
  providers: [AlertService]
})
export class CoreModule { }
