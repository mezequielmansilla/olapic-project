import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RoutesEnum } from '../shared/enums';

@Component({
  selector: 'op-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  openIssPhotos() {
    this.router.navigate([RoutesEnum.ISS_PHOTOS]);
  }

}
