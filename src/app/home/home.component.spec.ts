import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [HomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a description as `Select the application to load`', () => {
    const debugEl: HTMLElement = fixture.debugElement.nativeElement;
    const descriptionElement = debugEl.querySelector('.op-home__select-app-label');
    expect(descriptionElement).toBeTruthy();
    expect(descriptionElement.innerHTML).toBe('Select the application to load');
  });

  it('should have a button with the text `ISS Photos`', () => {
    const debugEl: HTMLElement = fixture.debugElement.nativeElement;
    const descriptionElement = debugEl.querySelector('button');
    expect(descriptionElement).toBeTruthy();
    expect(descriptionElement.innerHTML).toBe('ISS Photos');
  });
});
