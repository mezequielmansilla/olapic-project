
var ftpClient = require("ftp-client"),
  config = require('./ftp-config.json'),
  options = {
    logging: "basic"
  },
  client = new ftpClient(config, options);

client.connect(function() {
  client.upload(
    ["dist/olapic-project/**"],
    "/",
    {
      baseDir: "dist/olapic-project",
      overwrite: "older"
    },
    function(result) {
      console.log(result);
    }
  );
});
